<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|Route::get('/welcome', function () {
    //return view('welcome');
 return '<h1>welcome to rentalPage</h1>';

});
Route::get('/user', function () {
    //return view('welcome');
 return view('pages.user');

});
Route::get('/users/{id}/{name}', function ($id,$name) {
    //return view('welcome');
 return 'Este es el usuario '. $name . ' con el id '. $id;

});
*/

Route::get('/', 'PagesController@index');
Route::get('/user', 'PagesController@user');

Route::resource('userR','UserController');




