<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rentals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuarioR');
            $table->string('contraseña');
            $table->string('distrito');
            $table->string('abreviaciones');
            $table->string('direccion');
            $table->string('numeroDetarjeta');
            $table->string('cvv');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rentals');
    }
}
